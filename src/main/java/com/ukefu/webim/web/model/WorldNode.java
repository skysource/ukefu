package com.ukefu.webim.web.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class WorldNode implements Serializable {  
	  
    private static final long serialVersionUID = 1L;  
    private int value;         //敏感词的值  
    private List<WorldNode> childNodes;    //子敏感词  
    private boolean isLeaf;     //一条敏感词结束标识  
    private String orgi ;
    private String type ;
      
    public WorldNode(int value, boolean isLeaf , String orgi , String type) {  
        this.value = value;  
        this.isLeaf = isLeaf;  
        this.orgi = orgi  ;
        this.type = type ;
    }  
    public WorldNode() {  
    }  
    //添加子集合  
    public WorldNode addChildNode(int value,boolean isLeaf ,String orgi , String type) {  
        if(childNodes==null) {  
            childNodes = new ArrayList<WorldNode>();  
        }  
        for(WorldNode worldNode:childNodes) {  
            if(worldNode.value==value) {  
                worldNode.setLeaf(isLeaf);  
                return worldNode;  
            }  
        }  
        WorldNode newNode = new WorldNode(value,isLeaf , orgi , type);  
        childNodes.add(newNode);  
        return newNode;  
    }  
    //查询子集合  
    public WorldNode queryChildNode(int value) {  
        if(childNodes==null) {  
            return null;  
        }  
        for(WorldNode world:childNodes) {  
            if(world.value==value) {  
                return world;  
            }  
        }  
        return null;  
    }  
      
    public int getValue() {  
        return value;  
    }  
    public void setValue(int value) {  
        this.value = value;  
    }  
    public List<WorldNode> getChildNodes() {  
        return childNodes;  
    }  
    public void setChildNodes(List<WorldNode> childNodes) {  
        this.childNodes = childNodes;  
    }  
    public boolean isLeaf() {  
        return isLeaf;  
    }  
    public void setLeaf(boolean isLeaf) {  
        this.isLeaf = isLeaf;  
    }
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}  
