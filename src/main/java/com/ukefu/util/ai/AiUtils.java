package com.ukefu.util.ai;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.UKTools;
import com.ukefu.webim.service.repository.SceneItemRepository;
import com.ukefu.webim.web.model.AiConfig;
import com.ukefu.webim.web.model.AiUser;
import com.ukefu.webim.web.model.SceneItem;
import com.ukefu.webim.web.model.SysDic;
import com.ukefu.webim.web.model.UKeFuDic;

public class AiUtils {
	private static AiDicTrie aiDicTrie = new AiDicTrie();
	
	/**
	 * 初始化 AI语料库
	 * @param orgi
	 * @throws IOException
	 * @throws JcsegException 
	 */
	public static AiDicTrie init(String orgi) throws IOException{
		aiDicTrie.clean();
		SceneItemRepository sceneItemRes = UKDataContext.getContext().getBean(SceneItemRepository.class) ;
		List<SceneItem> sceneItemList = sceneItemRes.findByOrgiAndItemtype(orgi, UKDataContext.AiItemType.USERINPUT.toString()) ;
		for(SceneItem item : sceneItemList){
			if(!StringUtils.isBlank(item.getInputcon())) {
				for(String type : item.getInputcon().split(",")) {
					String[] types =  new String[] {type} ;
					for(String tp : types) {
						aiDicTrie.insertDic(tp , item.getSceneid());
					}
				}
			}else {
				aiDicTrie.insert(item.getContent(), item.getSceneid());
			}
		}
		return aiDicTrie;
	}
	
	public static AiDicTrie getAiDicTrie(){
		return aiDicTrie ;
	}
	
	
	/**
	 * AI配置
	 * @param orgi
	 * @return
	 */
	public static AiConfig initAiConfig(String aiid,String orgi){
		return UKTools.initAiConfig(aiid, orgi) ;
	}
	
	/**
	 * 机器人会话数据补全
	 * @param msg
	 * @param busslist
	 * @param bussop
	 * @param aiUser
	 */
	public static void processAiUserNames(String msg , String busslist , boolean bussop , AiUser aiUser) {
		if(bussop && !StringUtils.isBlank(busslist)) {
			String[] codes =  busslist.split(",") ;
			List<SysDic> dicList = UKeFuDic.getInstance().getDic("com.dic.xiaoe.input") ;
			for(SysDic dic : dicList) {
				for(String code : codes) {
					if(dic.getCode().equals(code)) {
						String value = msg ;
						if(!StringUtils.isBlank(msg)) {
							if(msg.length() > 255) {
								value = msg.substring(0, 255) ;
							}
							if(dic.isDiscode() && aiUser.getNames().get(code) != null) {
								value = aiUser.getNames().get(code) +","+ value ;
							}
							aiUser.getNames().put(code , value) ;
						}
						break ;
					}
				}
			}
		}
	}
}
